<?php
require 'parametros.class.php';
require_once '../controladores/configuracion.php';
include_once '../controladores/fr_funciones.inc.php';
class ConexionMysql extends ParametrosConexion{
/* ------variables de conexion--------------- */
    protected $_CONEXION; //objeto de tipo conexion cuando establece el dsn en caso contrario esta variable es un null
    protected $_QUERY; 
    private $_DEBUG,$_FILE,$_WRITTE,$_MODO;
            
    function __construct() 
    {
        $this->establecerConexion(); //genera automaticamente una conexion cuando se crea el objero de la clase
        $config = new Config_Ini();
        $this->_DEBUG = $config->getDebug();
        $this->_FILE = $config->getFilelog();
        $this->_WRITTE = $config->getWritelog();
        $this->_MODO = $config->getMododebug();
    }//fin del constructor
    
    protected function terminarConexion()
        {
            unset($this->_CONEXION);
            unset($this->_QUERY);
        }//finaliza el metodo para terminar las conexiones

        private function establecerConexion()
        {
            list($recurso,$user,$pass) = parent::ParametrosConexion();//estable una lista de parametros para generar el dsn
                try {//establece la conexion
                        $this->_CONEXION = new PDO($recurso,$user,$pass,array(PDO::ATTR_PERSISTENT => true , PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION )); 
                    } 
                    catch (PDOException $error)
                    { //manejo de errores
                        echo ($this->_DEBUG == 0) ? 'Existe un problema al intertar conectarse ala base de datos favor de verificar sus parametros':
                        $this->informacionDebug($error,$this->_MODO,$this->_WRITTE);//informacion especifica de errores
                        exit();
                    }       
        }//esta función establece la conexion hacia la base de datos
    
    
        protected function ejecutaConsultas($consulta,$array=null)
        {
            $this->_QUERY = $this->_CONEXION->prepare($consulta);
                try {
                    $this->_QUERY->execute($array);
                    return $this->_QUERY;
                    }//atrapando errores
                        catch (PDOException $error)
                        {
                         return $label = ($this->_DEBUG == 0) ? 'Existe un problema con la ejecución de la consulta , verifique el codigo SQL enviado ' : 
                         $this->informacionDebug($error,$this->_MODO,$this->_WRITTE); //informacion especifica de errores
                         exit();
                        }//fin de trycatch
        }//fin de la funcion que ejecuta Consultas realizadas con SQL
   
        protected function ejecutaProcedmientos(){}

        protected function ejecutaTransacciones(){}

        protected function informacionDebug($expecion,$modo,$write)
        {
            switch($modo)
            {
                case 1:
                        $log = $expecion->getMessage().PHP_EOL;
                            break;
                case 2:
                        $log = "El archivo : ".$expecion->getFile().PHP_EOL."Tiene el siguiente mensaje de error : ".$expecion->getMessage().
                        " en la linea ".$expecion->getLine().PHP_EOL; 
                            break;
                case 3:
                       $log = $expecion->getTraceAsString().PHP_EOL; 
                            break;
            }
            if($write!=0){$this->writteLog(/*date("Y-m-d H:i:s")." ".*/$log);} //problemas con la zona horaria se debe definir en el php.ini date.timezone
        return $log;    
        }// fin de la funcion 

        private function writteLog($informacion)
        {
          file_put_contents($this->_FILE, $informacion, FILE_APPEND | LOCK_EX);
        }//fin para la escritura de logs
       
        protected function getMensaje($say,$speak)
        {
              $utilerias = new MainFunciones();
             if(!is_object($say))
                {
                    return $speak = $utilerias->setMensajes('danger','Error de capa modelo ! ',$say);    
                }
                else
                    {
                        return $speak=$utilerias->setMensajes('success','Satisfactoriamente ! ',$speak);
                    }
    }//fin de mensajes visualuizacion
   //--inicio de los metodos abstractos heredados de la inteface de claves conexiion
    protected function setPassword($password)
    {}
    protected function setPuerto($puerto)
    {}
    protected function setUsuario($usuario) 
    {}
    protected function setBaseDatos($basedatos)
    {}
    protected function setServidor($servidor)
    {}
    //-- fin de la implementacion metodos abastractos
    
}//fin de la clase conector Mysql

?>

