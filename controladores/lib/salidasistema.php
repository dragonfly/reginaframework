<?php
  session_start();
  include_once '../controladores/seguridad.class.php';
  $seguridad = new AccesoSistema;
  $seguridad->cerrarSesion();   
?>
<!DOCTYPE html>
<html lang="es">
  <head>
    <title>Salida</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="refresh" content="5;url=../">
    <link rel="shortcut icon" href="../logo.ico" type="image/x-icon" />
    <link rel="stylesheet" type="text/css" href="../utilerias/css/bootstrap.css">
  </head>
  <body>
    <section class="container text-center">
      <section class="jumbotron">
        <figure>
          <img src="../utilerias/img/logo_opt">
          <figcaption><h3><img src="../utilerias/img/succes.png"> Usted ha salido del sistema satisfactoriamente</h3></figcaption>   
        </figure> 
      </section>
    </section>
  </body>
</html>