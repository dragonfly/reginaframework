<?php 
require_once'configuracion.php';
class MainPlantilla extends Config_Ini 
{
	private $_PATH=self::_PATH;
	private $_HOME=null;
    private $_SALIDA=self::_SALIDA;
    protected $_CREDENCIAL=null;
	public $_TITLE=null;
	public  $_HEADER=null;
    public  $_LIBRERIAS=null;
    
	function __construct($title,$home,$credencial)
    { 
		 $this->getLibrerias();
		 $this->_HOME=$home.".php";
		 $this->_TITLE=$title;
         $this->_CREDENCIAL=$credencial;
		 $this->setHeader();
	}

	private function setHeader(){
	     $this->_HEADER="<nav class='navbar navbar-default'>".
	     $this->makeMenu()."</nav>";
	}

	private function  makeMenu()
    {
	  return $menu="
    <div class='container-fluid'>
        <div class='navbar-header'>
            <button type='button' class='navbar-toggle' data-toggle='collapse' data-target='#bs-example-navbar-collapse-1'>
                <span class='sr-only'></span>
                <span class='icon-bar'></span>
                <span class='icon-bar'></span>
                <span class='icon-bar'></span>
            </button>
            <a class='navbar-brand' href='$this->_HOME'><img src='../utilerias/img/logomin.png'> <span style='color:black'> ICTC </span> | $this->_TITLE</a>
        </div>
            <div class='collapse navbar-collapse' id='bs-example-navbar-collapse-1'>
                <ul class='nav navbar-nav navbar-right'>".
                    $this->makeDropdown()
                ."</ul>
            </div>
    </div>";
	}
    
    private function makeDropdown()
    {
     return $dropdown="<li><a href='#'><img src='".$this->_CREDENCIAL['foto']."' style='width:40px; heigth:40px;' ></a></li>
                    <li class='dropdown'>
                    <a href='#' class='dropdown-toggle' data-toggle='dropdown'>".$this->_CREDENCIAL['nombre']."<b class='caret'></b></a>
                        <ul class='dropdown-menu'>
                            <li><a href='#'><span class='glyphicon glyphicon-bookmark'></span> ".$this->_CREDENCIAL['puesto']."</a></li>
                            <li><a href='#'><span class='glyphicon glyphicon-star-empty'></span> ".$this->_CREDENCIAL['privilegio']."</a></li>
                            <li><a href='#'><span class='glyphicon glyphicon-envelope'></span> Mensajes</a></li>
                            <li class='divider'></li>
                            <li><a href='$this->_SALIDA'><span class='glyphicon glyphicon-off'></span> Cerrar Sesion</a></li>
                        </ul>
                    </li>";   
    }

    private function getLibrerias()
    {
         $this->_LIBRERIAS = 
              " <link rel='shortcut icon' href='../logo.ico' type='image/x-icon' />
               <link href='$this->_PATH/css/bootstrap.css' rel='stylesheet'>
               <link href='$this->_PATH/css/bootstrap-responsive.css' rel='stylesheet'>
               <link href='$this->_PATH/css/bootstrap-datetimepicker.min.css' rel='stylesheet'>
               <script src='$this->_PATH/lib/jquery.js'></script>
               <script src='$this->_PATH/lib/bootstrap.min.js'></script>
               <script src='$this->_PATH/lib/moment.js'></script>
               <script src='$this->_PATH/lib/sistemamensajes.js'></script>
               <script src='$this->_PATH/lib/bootstrap-datetimepicker.min.js'></script>
               <script src='$this->_PATH/lib/jquery.ui.effect.min.js'></script>";
	}    
    
    public function setMenu($privilegio){
        parent::getModulos($privilegio,$this->_TITLE);
        
    }//fin clase construye menu
    
}//fin de las clases
?>